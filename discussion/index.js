// in JS basically it has semicolon at the end of the statement for it to work. Not required but best practice.
		
// alert ("Hello World");

// console.log("Hello World");
// console.log(3+2);

// VARIABLES

let name = "Zuitt";
name = "Emmanuel";
console.log(name);

let age = 22;

const boilingPoint = 100;
// boilingPoint = 200;
console.log (boilingPoint);
console.log (age + '3');
console.log (name + 3)

// BOOLEAN: Either true or false
let isAlive = true;
console.log (isAlive);

// ARRAYS - items are usually put in an array to group them together

let grades = [98, 96, 95, 90];

let grades1 = 98;
let grades2 = 96;
let grades3 = 95;
let grades4 = 90;

grades[10] = 200;

console.log(grades);
let movie = "jaws"
// console.log(song);

let isp;
console.log(isp);


//null vs undefined

// Undefined - represents the sate of a variable that has been declared but without value

// null - used intentionally to express the absence of a value in a declared/initalized variable


let spouse = null;
console.log(spouse);

// Objects - special type of data types that is used to mimic real world objects/items

/*
	Syntax:

	let objectName = { 
	
		property1: keyValue1, 
		property2: keyValue2,
		property3: keyValue3
	}
*/

let myGrades = {
	firstGrading: 98,
	secondGrading: 92,
	thirdGrading: 90,
	fourthGrading: 94.6
}

console.log(myGrades);


let person = {
	fullName: 'Alonzo Cruz',
	age: 25,
	isMarried: false,
	contact: ['0912312413412', '09123011123'],
	address: {
		houseNumber: 345,
		city: 'Manila'
	}
};

console.log(person);