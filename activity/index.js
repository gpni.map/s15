console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	console.log("Activity 1:");


	const firstName = "Glenn";
	console.log("First Name - " + firstName);

	const lastName = "Perey";
	console.log("Last Name - " + lastName);

	let age = 36;
	console.log("Age - " + age);

	let hobbies = ['playing guitar', 'watching movies', 'reading manga'];
	console.log("Hobbies - " + hobbies);

	let workAddress = {
		houseNumber: 790,
		street: "P. Ocampo Street",
		city: "Manila",
		state: "Philippines"
	};

	console.log("Work address" + workAddress);







/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	console.log("Activity 2:");

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40; // from age to currentAge
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriendName = "Bucky Barnes"; // fullName to bestFriendName
	console.log("My bestfriend is: " + bestFriendName);

	let lastLocation = "Arctic Ocean"; // change const to let
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
